#SWEN30007 Group 7 Readme

##Overview.
This system is created for the final semester project in the Bachelor of Science (Software Systems) at The University of Melbourne. The system is designed to allow persons in need of assistance to notify their carers that they are in need of help.

##Building.
Open MediCall/MediCall.xcworkspace in XCode and RUN the MediCall project in the simulator.

##Features
MediCall offers this following features

1. Calling Functionality
	* User will be able to initiate calls, receive calls and decline calls
2. Account Management Functionality
	* User able to sign in, sign out, and sign up
	* User able to edit their profile and change their passwords
3. Managing Contact Functionality
	* Adding/Editing Contact
	* Managing Carer (for Assisted User only)

##Testing
1. 	Unit Test
	* navigate to Product > Test (⌘U)
2.	End-to-End Test
	* navigate to Product > Profile (⌘I)
	* choose UIAutomation
	* Import particular .js files that want to be tested from MediCall/MediCallTests/JavaScript Files/
	* Press record (⌘R)
	
