//End-to-End Test for Login Scenario

//Initialize the window
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();
target.logElementTree();

//Initialize the field value
var emailField = window.textFields()[0];
var passwordField = window.secureTextFields()[0];
var signInButton = window.buttons()[1];
var forgotPasswordButton = window.buttons()[0];
var signUpButton = window.buttons()[2];

//value for email and password
var validUsername = "awijaya";
var wrongPassword = "wrongpassword";
var validPassword = "admin"

function doLogin(username, password)
{
	//FAILURE LOGIN CASE (User input Invalid Password)
	//Tap the emailField, and fill up the value
	emailField.tap();
	emailField.setValue(username); 
	UIALogger.logMessage("LOG: INPUT Email_address");


	//Tap outside the textfield, wait for 2 seconds
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside Textfield");
	target.delay(2);
	
	//Tap the passwordField and fill up the value
	passwordField.tap();
	passwordField.setValue(password);
	UIALogger.logMessage("LOG: INPUT Valid password");

	//Tap outside the textfield, wait for 1 second
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside textfield");
	target.delay(1);
	
	//Tap signInButton
	UIALogger.logMessage("LOG: TAP SignIn button");
	signInButton.tap();
}

//Failure Login Scenario
doLogin(validUsername, wrongPassword);
window.buttons().triggerAlertButton;
UIALogger.logMessage("LOG: Dismiss the alert");

//Succees Login Scenario
doLogin(validUsername, validPassword);
UIALogger.logMessage("LOG: ENTER HOME VIEW UI");

target.delay(3);

var settingButton = window.tabBar().buttons()[3];
settingButton.tap();

target.delay(2);
var signOutButton = window.buttons()[3];
signOutButton.tap();
