//End-to-End Test for Manage Carer Scenario

//Initialize the window
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();
target.logElementTree();

//Initialize the field value
var emailField = window.textFields()[0];
var passwordField = window.secureTextFields()[0];
var signInButton = window.buttons()[1];
var forgotPasswordButton = window.buttons()[0];
var signUpButton = window.buttons()[2];

//value for email and password
var validUsername = "vano";
var wrongPassword = "wrongpassword";
var validPassword = "vilberto"

//value for contact
var primaryFirstName = "Aristea"
var primaryLastName = "Wijaya"
var primaryPhoneNumber = "+61420971993"
var secondaryFirstName = "Christian"
var secondaryLastName = "Umboh"
var secondaryPhoneNumber = "+61433371222" 

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

function doLogin(username, password)
{
	//FAILURE LOGIN CASE (User input Invalid Password)
	//Tap the emailField, and fill up the value
	emailField.tap();
	emailField.setValue(username); 
	UIALogger.logMessage("LOG: INPUT Email_address");


	//Tap outside the textfield, wait for 2 seconds
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside Textfield");
	target.delay(1);
	
	//Tap the passwordField and fill up the value
	passwordField.tap();
	passwordField.setValue(password);
	UIALogger.logMessage("LOG: INPUT Valid password");

	//Tap outside the textfield, wait for 1 second
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside textfield");
	target.delay(1);
	
	//Tap signInButton
	UIALogger.logMessage("LOG: TAP SignIn button");
	signInButton.tap();
}

//Succees Login Scenario
doLogin(validUsername, validPassword);
UIALogger.logMessage("LOG: ENTER HOME VIEW UI");

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

//Entering Setting View
target.delay(3);
var settingButton = window.tabBar().buttons()[3];
settingButton.tap();
UIALogger.logMessage("LOG: Setting Element Tree");
target.logElementTree();

//Tapping the Manage Carers
target.delay(3);
var manageCarerButton = window.buttons()[2];
manageCarerButton.tap()
UIALogger.logMessage("LOG: ManageCarer Element Tree");
target.logElementTree();

//Tapping the Edit button
target.delay(2)
var editButton = window.navigationBar().buttons()[1];
editButton.tap();

//Tapping the Add button
target.delay(2);
var addButton = window.tableViews()[0].navigationBar().buttons()[1];
addButton.tap();

target.delay(2);
var firstNameField = window.scrollViews()[0].textFields()[0];
firstNameField.tap();
firstNameField.setValue(primaryFirstName);

target.delay(1);
var lastNameField = window.scrollViews()[0].textFields()[1];
lastNameField.tap();
lastNameField.setValue(primaryLastName);

target.tap({x:150, y:20});
target.delay(1);
var phoneNumberField = window.scrollViews()[0].textFields()[2];
phoneNumberField.tap();
phoneNumberField.setValue(primaryPhoneNumber);

//Tapping the Save Button and dismiss the alert
target.tap({x:150, y:20});
target.delay(2);
var saveButton = window.scrollViews()[0].buttons()[0];
saveButton.tap();

//Entering Setting View
target.delay(3);
var settingButton = window.tabBar().buttons()[3];
settingButton.tap();
UIALogger.logMessage("LOG: Setting Element Tree");
target.logElementTree();

//Tapping the Manage Carers
target.delay(3);
var manageCarerButton = window.buttons()[2];
manageCarerButton.tap()
UIALogger.logMessage("LOG: ManageCarer Element Tree");
target.logElementTree();

//Tapping the Edit button
target.delay(2);
var editButton = window.navigationBar().buttons()[1];
editButton.tap();

target.delay(1);
var contactCell = window.tableViews()[0].cells()[0];
contactCell.tap();

target.delay(3);
target.logElementTree();
var setPrimaryButton = window.scrollViews()[0].buttons()[1];
setPrimaryButton.tap();
window.buttons().triggerAlertButton;

target.delay(1);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//Tapping the Add button
target.delay(2);
var addButton = window.tableViews()[0].navigationBar().buttons()[1];
addButton.tap();

target.delay(2);
var firstNameField = window.scrollViews()[0].textFields()[0];
firstNameField.tap();
firstNameField.setValue(secondaryFirstName);

target.delay(1);
var lastNameField = window.scrollViews()[0].textFields()[1];
lastNameField.tap();
lastNameField.setValue(secondaryLastName);

target.tap({x:150, y:20});
target.delay(1);
var phoneNumberField = window.scrollViews()[0].textFields()[2];
phoneNumberField.tap();
phoneNumberField.setValue(secondaryPhoneNumber);

//Tapping the Save Button and dismiss the alert
target.tap({x:150, y:20});
target.delay(2);
var saveButton = window.scrollViews()[0].buttons()[0];
saveButton.tap();

target.delay(1);
var contactCell = window.tableViews()[0].cells()[1];
contactCell.tap();

target.delay(3);
target.logElementTree();
var setPrimaryButton = window.scrollViews()[0].buttons()[3];
setPrimaryButton.tap();
window.buttons().triggerAlertButton;

//Entering Setting View
target.delay(3);
var settingButton = window.tabBar().buttons()[3];
settingButton.tap();
UIALogger.logMessage("LOG: Setting Element Tree");
target.logElementTree();

//Tapping the Manage Carers
target.delay(3);
var manageCarerButton = window.buttons()[2];
manageCarerButton.tap()
UIALogger.logMessage("LOG: ManageCarer Element Tree");
target.logElementTree();

target.delay(1);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//Tapping the Sign Out Button
target.delay(2);
var signOutButton = window.buttons()[3];
signOutButton.tap();
