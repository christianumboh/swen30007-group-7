//End-to-End Test for All Segues and Buttons Scenario

//Initialize the window
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var window = app.mainWindow();
target.logElementTree();

//Initialize the field value
var emailField = window.textFields()[0];
var passwordField = window.secureTextFields()[0];
var signInButton = window.buttons()[1];
var forgotPasswordButton = window.buttons()[0];
var signUpButton = window.buttons()[2];

//value for email and password
var validUsername = "vano";
var wrongPassword = "wrongpassword";
var validPassword = "vilberto";

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

function doLogin(username, password)
{
	//FAILURE LOGIN CASE (User input Invalid Password)
	//Tap the emailField, and fill up the value
	emailField.tap();
	emailField.setValue(username); 
	UIALogger.logMessage("LOG: INPUT Email_address");

target.tap({x:58.00, y:130.00});
	
	//Tap outside the textfield, wait for 2 seconds
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside Textfield");
	target.delay(2);
	
	//Tap the passwordField and fill up the value
	passwordField.tap();
	passwordField.setValue(password);
	UIALogger.logMessage("LOG: INPUT Valid password");

	//Tap outside the textfield, wait for 1 second
	target.tap({x:100, y:100});
	UIALogger.logMessage("LOG: TAP Outside textfield");
	target.delay(1);
	
	//Tap signInButton
	UIALogger.logMessage("LOG: TAP SignIn button");
	signInButton.tap();
}

//Succees Login Scenario
doLogin(validUsername, validPassword);
UIALogger.logMessage("LOG: ENTER HOME VIEW UI");

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

//Entering Contact List View
target.delay(4);
var contactListButton = window.tabBar().buttons()[1];
contactListButton.tap();
UIALogger.logMessage("LOG: ContactList Element Tree");
target.logElementTree();

//Tapping the Add button
target.delay(2);
var addButton = window.tableViews()[0].navigationBar().buttons()[1];
addButton.tap();

//Tapping the Save Button and dismiss the alert
target.delay(2);
var saveButton = window.scrollViews()[0].buttons()[0];
saveButton.tap();
window.buttons().triggerAlertButton;
UIALogger.logMessage("LOG: Dismiss the alert");

//Tapping the Back Button in New Contact
target.delay(2);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

//Entering Notification View
target.delay(3);
var notifButton = window.tabBar().buttons()[2];
notifButton.tap();
UIALogger.logMessage("LOG: Notification Element Tree");
target.logElementTree();

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------

//Entering Setting View
target.delay(3);
var settingButton = window.tabBar().buttons()[3];
settingButton.tap();
UIALogger.logMessage("LOG: Setting Element Tree");
target.logElementTree();

//***********************************

//Tapping the Edit Profile
target.delay(5);
var editProfileButton = window.buttons()[0];
editProfileButton.tap();
UIALogger.logMessage("LOG: EditProfile Element Tree");
target.logElementTree();

//Tapping the Save Button and dismiss the alert
target.delay(5);
var saveButton = window.scrollViews()[0].buttons()[0];
saveButton.tap();

//Go back to Setting Screen View and enter the Profile again, 
//to test the back button inside Edit Profile
target.delay(2);
settingButton.tap();
target.delay(2);
editProfileButton.tap();

//Tapping the Back Button inside Edit Profile
target.delay(2);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//***********************************

//Tapping the Change Password
target.delay(2);
var changePasswordButton = window.buttons()[1];
changePasswordButton.tap()
UIALogger.logMessage("LOG: ChangePassword Element Tree");
target.logElementTree();

//Tapping the Save Button and dismiss the alert
target.delay(5);
var saveButton = window.buttons()[0];
saveButton.tap();
window.buttons().triggerAlertButton;
UIALogger.logMessage("LOG: Dismiss the alert");

//Tapping the Back Button inside Change Password
target.delay(2);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//***********************************

//Tapping the Manage Carers
target.delay(2);
var manageCarerButton = window.buttons()[2];
manageCarerButton.tap()
UIALogger.logMessage("LOG: ManageCarer Element Tree");
target.logElementTree();

//Tapping the Edit button inside Manage Carers
target.delay(5);
var editButton = window.navigationBar().buttons()[1];
editButton.tap();

//Tapping the Back Button inside the Edit Manage Carers
target.delay(2);
var backButton = window.tableViews()[0].navigationBar().buttons()[0];
backButton.tap();

//Tapping the Back Button inside ManageCarer
target.delay(2);
var backButton = window.navigationBar().buttons()[0];
backButton.tap();

//***********************************

//Tapping the Sign Out Button
target.delay(2);
var signOutButton = window.buttons()[3];
signOutButton.tap();

//----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------
