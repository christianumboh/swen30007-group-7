#SWEN30007 Group 7, Simple Testing Readme

##Overview.
The simple-testing are implemented on MediCallTests.m, the unit being tested is the LoginValidator.m,
the testing is just simply by using assert true and false, to check whether the behaviour of the thread.

The end-to-end test are implemented on the ViewController (Login Screen UI), the test includes 2 scenario.
Scenario 1 : Failure login due to invalid password input, Scenario 2: Succeed login, that will then take
the user to HomeViewController (Home Screen UI).

##How to Run the test.
For Unit Test of Login Validator.
Open the MediCall.xcworkspace, then choose Product > Test.

For End-to-End test of Login Scenario
- simply navigate to ~/MediCall/MediCallTests then open the End-to-End_Login_Scenario_Test.trace
- press the record button or simply press ⌘R.
