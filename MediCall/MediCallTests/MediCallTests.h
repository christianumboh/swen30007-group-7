//
//  MediCallTests.h
//  MediCallTests
//
//  Created by Christian Umboh on 20/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
#import "ProfileViewController.h"
#import "ChangePasswordViewController.h"
#import "ContactAddViewController.h"
#import "UserObject.h"

@interface MediCallTests : SenTestCase
@property (strong,nonatomic) ProfileViewController *profileValidator;
@property (strong,nonatomic) ChangePasswordViewController *changePasswordValidator;
@property (strong,nonatomic) ContactAddViewController *contactAddValidator;
@end
