//
//  MediCallTests.m
//  MediCallTests
//
//  Created by Christian Umboh on 20/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "MediCallTests.h"

@interface MediCallTests()
@end

@implementation MediCallTests
@synthesize profileValidator = _profileValidator;
@synthesize changePasswordValidator = _changePasswordValidator;
@synthesize contactAddValidator = _contactAddValidator;

- (ProfileViewController *)profileValidator
{
    if(!_profileValidator)
    {
        _profileValidator = [[ProfileViewController alloc] init];
    }
    return _profileValidator;
}

- (ChangePasswordViewController *)changePasswordValidator
{
    if(!_changePasswordValidator)
    {
        _changePasswordValidator = [[ChangePasswordViewController alloc] init];
    }
    return _changePasswordValidator;
}

- (ContactAddViewController *)contactAddValidator
{
    if(!_contactAddValidator)
    {
        _contactAddValidator = [[ContactAddViewController alloc] init];
    }
    return _contactAddValidator;
}

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    /*Unit Test for Edit Profile functionality
     *It will test the validateEditProfile function, this function is the core for editing the profile.
     *This function will save the user input to a userobject, and then check whether the userobject value is the same as the user input
     *This function is the core because to the information that will be transferred to PARSE database is the value from userobject.
     *Hence the correctness of values inside userobject is really crucial.
     */
    STAssertFalse([self.profileValidator validateEditProfile:@"" andLastName:@"Wijaya" andDateOfBirth:@"26-Jan-1993" andAddress:@"testing address"], @"Zero length for FirstName");
    STAssertFalse([self.profileValidator validateEditProfile:@"Aristea" andLastName:@"" andDateOfBirth:@"26-Jan-1993" andAddress:@"testing address"], @"Zero length for LastName");
    STAssertFalse([self.profileValidator validateEditProfile:@"Aristea" andLastName:@"Wijaya" andDateOfBirth:@"" andAddress:@"testing address"], @"Zero length for DateOfBirth");
    STAssertFalse([self.profileValidator validateEditProfile:@"Aristea" andLastName:@"Wijaya" andDateOfBirth:@"26-Jan-1993" andAddress:@""], @"Zero length for User Address");
    STAssertTrue([self.profileValidator validateEditProfile:@"Aristea" andLastName:@"Wijaya" andDateOfBirth:@"26-Jan-1993" andAddress:@"2901/63 Whiteman Street"], @"Correct Information for User Profile");
    
    /*Unit Test for Change Password functionality
     *It will test the checkPassword function, this functions check whether the two field values for new password, and the re-type
     *new password. If the two field has the same value then it will return true, else return false. (It's case-sensitive)
     */
    STAssertFalse([self.changePasswordValidator checkPassword:@"test" andRetypePassword:@"test1"], @"Different password input.");
    STAssertFalse([self.changePasswordValidator checkPassword:@"test" andRetypePassword:@"Test"], @"Different password input, one using uppercase letter");
    STAssertTrue([self.changePasswordValidator checkPassword:@"test" andRetypePassword:@"test"], @"Same password input.");
    //STFail(@"Unit tests are not implemented yet in MediCallTests");
    
    /*Unit Test for New Contact functionality*/ 
    STAssertFalse([self.contactAddValidator validateNewContact:@"" andLastName:@"Umboh" andPhoneNumber:@"0433371222"], @"Zero length for FirstName");
    STAssertFalse([self.contactAddValidator validateNewContact:@"Christian" andLastName:@"" andPhoneNumber:@"0433371222"], @"Zero length for LastName");
    STAssertFalse([self.contactAddValidator validateNewContact:@"Christian" andLastName:@"Umboh" andPhoneNumber:@""], @"Zero length for PhoneNumber");
    STAssertTrue([self.contactAddValidator validateNewContact:@"Christian" andLastName:@"Umboh" andPhoneNumber:@"0433371222"], @"Correct Information for New Contact");
}

@end
