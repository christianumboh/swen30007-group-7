//
//  ContactDetailViewController.m
//  MediCall
//
//  Created by Christian Umboh on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ContactDetailViewController.h"

@interface ContactDetailViewController ()

@end

@implementation ContactDetailViewController
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNumber;
@synthesize carerName;
@synthesize contact;
@synthesize saveButton;
@synthesize delButton;
@synthesize primaryCarerButton;
@synthesize secondaryCarerButton;
@synthesize callButton;
@synthesize scrollView;

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

/*
 * Function that is triggered when the "Back" button is tapped
 */
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Function that is triggered when the "Edit" button is tapped
 */
- (IBAction)edit: (id)sender
{
    // Allows editing on the text fields
    firstName.userInteractionEnabled = TRUE;
    lastName.userInteractionEnabled = TRUE;
    phoneNumber.userInteractionEnabled = TRUE;
    
    // Makes the save and delete buttons appear
    saveButton.hidden = FALSE;
    delButton.hidden = FALSE;
    
    // Hides the "Set as primary", "Set as secondary", and "Call" buttons
    primaryCarerButton.hidden = TRUE;
    primaryCarerButton.userInteractionEnabled = FALSE;
    secondaryCarerButton.hidden = TRUE;
    secondaryCarerButton.userInteractionEnabled = FALSE;
    callButton.hidden = TRUE;
    callButton.userInteractionEnabled = FALSE;
}

/*
 * Function that handles the saving of contact details
 * Triggered when the "Save" button is tapped
 */
- (IBAction)save:(id)sender {
    
    // Checks if the fields are not empty
    if( ![firstName.text isEqualToString:@""] && ![lastName.text isEqualToString:@""] && ![phoneNumber.text isEqualToString:@""])
    {
        NSManagedObjectContext *context = [self managedObjectContext];
    
        // Update existing contact
        [contact setValue:firstName.text forKey:@"firstName"];
        [contact setValue:lastName.text forKey:@"lastName"];
        [contact setValue:phoneNumber.text forKey:@"phoneNumber"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
    
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Missing Information!" message:@"Please ensure to fill in all the details." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [message show];
    }
}

/*
 * Function that handles contact deletion
 * Triggered when the "Delete" button is tapped
 */
- (IBAction)del:(id)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Delete object from database
    [context deleteObject:self.contact];
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
        return;
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [firstName setText:[contact valueForKey:@"firstName"]];
    [lastName setText:[contact valueForKey:@"lastName"]];
    [phoneNumber setText:[contact valueForKey:@"phoneNumber"]];
    
    // Show the "Set as primary", "Set as secondary", and "Call" buttons
    primaryCarerButton.hidden = FALSE;
    primaryCarerButton.userInteractionEnabled = TRUE;
    secondaryCarerButton.hidden = FALSE;
    secondaryCarerButton.userInteractionEnabled = TRUE;
    callButton.hidden = FALSE;
    callButton.userInteractionEnabled = TRUE;
    
    // Disable the editing on text fields
    firstName.userInteractionEnabled = FALSE;
    lastName.userInteractionEnabled = FALSE;
    phoneNumber.userInteractionEnabled = FALSE;
    
    //dismiss keyboard when tapping outside textfield
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackgroundClick:(id)sender
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [phoneNumber resignFirstResponder];
}

/*
 * Function that handles primary carer selection
 * Triggered when the "Set as Primary" button is tapped
 */
- (IBAction)setAsPrimary:(id)sender
{
    // Loads the user profile
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    
    // Set the primary carer values in database
    [profile setObject:firstName.text forKey:@"primarycarer"];
    [profile setObject:phoneNumber.text forKey:@"primarycarerphone"];
    [profile save];
    
    // Displays a success message if primary carer is successfully changed
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Primary carer changed." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
    [alert show];
}

/*
 * Function that handles secondary carer selection
 * Triggered when the "Set as Secondary" button is tapped
 */
- (IBAction)setAsSecondary:(id)sender
{
    // Loads the user profile
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    
    // Set the secondary carer values in database
    [profile setObject:firstName.text forKey:@"secondarycarer"];
    [profile setObject:phoneNumber.text forKey:@"secondarycarerphone"];
    [profile save];
    
    // Displays a success message if secondary carer is successfully changed
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Secondary carer changed." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
    [alert show];
}

/*
 * Function that handles the calling of a particular contact
 * Triggered when the "Call" button is tapped
 */
- (IBAction)callContact:(id)sender
{
    NSMutableString *phoneURL = [NSMutableString stringWithString:@"tel://"];
    [phoneURL appendString:[contact valueForKey:@"phoneNumber"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURL]];
    NSLog(@"%@", phoneURL);
}

- (void)dismissKeyboard
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [phoneNumber resignFirstResponder];
}

@end
