//
//  UserObject.h
//  MediCall
//
//  Created by Aristea Wijaya on 10/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserObject : UIViewController
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *dateOfBirth;
@property (nonatomic) NSString *address;
@property (nonatomic) NSString *phoneNumber;
@end
