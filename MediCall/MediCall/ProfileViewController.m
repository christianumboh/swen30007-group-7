//
//  ProfileViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()
@property (strong, nonatomic) UserObject* userObject;
@end

@implementation ProfileViewController
@synthesize userObject = _userObject; //the mock object from UserObject.m
@synthesize backButton; //button to dismiss the ProfileViewController
@synthesize saveButton; //button to save the changes made in Profile
@synthesize firstName; //textfield for First Name
@synthesize lastName; //textfield for Last Name
@synthesize address; //textfield for Address
@synthesize dateOfBirth; //textfield for Date of Birth
@synthesize userTypeButton; //Segmented button for type of User; carer or assisted
@synthesize scrollView; //ScrollView so that the View can be scrolled.
@synthesize datepick; //Date Picker view to help in putting input for date of birth
@synthesize doneDOB; //button to save the changes in date picker
@synthesize cancelDOB; //button to cancel changes in date picker
@synthesize buttonDOB; //button that trigger the Date Picker Visibility
@synthesize buttonBackground; //background image behind the Date Picker's button


//Initialize the Mock User Object
- (UserObject *)userObject
{
    if(!_userObject)
    {
        _userObject = [[UserObject alloc] init];
    }
    return _userObject;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Get the current user profile from Parse
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    NSString *firstNameValue = [profile objectForKey:@"firstname"];
    NSString *lastNameValue = [profile objectForKey:@"lastname"];
    NSString *addressValue = [profile objectForKey:@"address"];
    NSString *userType = [profile objectForKey:@"usertype"];
    NSString *dateOfBirthValue = [profile objectForKey:@"dateofbirth"];
    
    //Put the value of each fields,
    //If the there is a value in Parse profile display it in the profile view controller
    //Else user the field's placeholder value as a temporary value
    firstName.text = firstNameValue.length > 0? firstNameValue : firstName.placeholder;
    lastName.text = lastNameValue.length > 0? lastNameValue : lastName.placeholder;
    address.text = addressValue.length > 0? addressValue : address.placeholder;
    dateOfBirth.text = dateOfBirthValue.length > 0?  dateOfBirthValue : @"Enter your Date of Birth here.";
    
    //dismiss keyboard when tapping outside textfield
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    //Checking the user type to be highlighted in the profile
    //If the user type hasn't been chosen before, user can do it only once
    if(userType.length > 0)
    {
        if([userType isEqualToString:@"Carer"])
        {
            userTypeButton.selectedSegmentIndex = 0;
        }
        else
        {
            userTypeButton.selectedSegmentIndex = 1;
        }
        userTypeButton.userInteractionEnabled = FALSE;
        
    }
    else
    {
        [userTypeButton addTarget:self
                           action:@selector(chooseUserType:)
                 forControlEvents:UIControlEventValueChanged];
    }
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)dismissKeyboard
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [dateOfBirth resignFirstResponder];
    [address resignFirstResponder];
}

//This function handle the pushing of the all the profile information to the PARSE
//It will only push the information to PARSE if the information supplied are valid.
- (void)doSave
{
    BOOL validInformation = [self validateEditProfile:firstName.text andLastName:lastName.text andDateOfBirth:dateOfBirth.text andAddress:address.text];
    
    if (validInformation)
    {
        PFUser *currentUser = [PFUser currentUser];
        PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
        [query whereKey:@"username" equalTo:currentUser.username];
        PFObject *profile = [query getFirstObject];
        [profile setObject:self.userObject.firstName forKey:@"firstname"];
        [profile setObject:self.userObject.lastName forKey:@"lastname"];
        [profile setObject:self.userObject.dateOfBirth forKey:@"dateofbirth"];
        [profile setObject:self.userObject.address forKey:@"address"];
        [profile save];
        [self performSegueWithIdentifier:@"enterHome" sender:nil];
    }
}

//This function handle the validity of the information supplied for the user profile
//The information are valid if and only if all the supplied information for each fields contain no empty string
-(BOOL)validateEditProfile:(NSString *)inputFirstname andLastName:(NSString *)inputLastname andDateOfBirth:(NSString *)inputDateofbirth andAddress:(NSString *)inputAddress
{
    if(inputFirstname.length > 0 && inputLastname.length > 0 && inputDateofbirth.length > 0 && inputAddress.length > 0)
    {
        self.userObject.firstName = inputFirstname;
        self.userObject.lastName = inputLastname;
        self.userObject.dateOfBirth = inputDateofbirth;
        self.userObject.address = inputAddress;
        
        if([inputFirstname isEqualToString:self.userObject.firstName] &&
           [inputLastname isEqualToString:self.userObject.lastName] &&
           [inputAddress isEqualToString:self.userObject.address] &&
           [inputDateofbirth isEqualToString:self.userObject.dateOfBirth])
        {
            return TRUE;
        }
        else
            return FALSE;
    }
    else
        return FALSE;
}

//Dismiss the keyboard if the user tap things other than text fields
- (IBAction)backgroundClick:(id)sender
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [dateOfBirth resignFirstResponder];
    [address resignFirstResponder];
}

//This function handle the action given by the saveProfile Button
//It will trigger the doSave function if all the profile fields is filled up and user type is chosen
//Else show error messages depending on what are the error.
- (IBAction)saveProfile:(id)sender
{
    if(userTypeButton.selectedSegmentIndex == UISegmentedControlNoSegment)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Specify User Type" message:@"Please ensure that you have selected your user type (Carer/Assisted). This can only be done once." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
        [alert show];
    }
    
    else
    {
        if(firstName.text.length > 0 &&  lastName.text.length > 0 && address.text.length > 0 && dateOfBirth.text.length > 0)
        {
            [self performSelector:@selector(doSave)];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Field Information" message:@"Please ensure every field is filled before you save the profile." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
            [alert show];
        }
    }
    
}

//Function that handle the action/selection on the UserType Segmented button
//It will also push the value of the segmented button to user's parse profile
- (IBAction)chooseUserType:(id)sender
{
    NSString *userType;
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if(selectedSegment == 0)
    {
        userType = @"Carer";
    }
    else
    {
        userType = @"Assisted";
    }
    
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    [profile setObject:userType forKey:@"usertype"];
    [profile save];
}

//Function that handle the cancelation of editing profile
//The user cannot dismiss the profile view controller, if all the value of the user's parse profile is still empty
- (IBAction)cancelEdit:(id)sender
{
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    NSString *firstNameValue = [profile objectForKey:@"firstname"];
    NSString *lastNameValue = [profile objectForKey:@"lastname"];
    NSString *addressValue = [profile objectForKey:@"address"];
    NSString *dateOfBirthValue = [profile objectForKey:@"dateofbirth"];
    
    if(firstNameValue.length > 0 &&  lastNameValue.length > 0 && addressValue.length > 0 && dateOfBirthValue.length > 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Field Information" message:@"Please ensure every field is filled before you save the profile." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
        [alert show];
    }
}

//Trigger the Date Picker
- (IBAction) buttonDOB:(id)sender
{
    datepick.hidden = FALSE;
    doneDOB.hidden = FALSE;
    cancelDOB.hidden = FALSE;
    buttonBackground.hidden = FALSE;
}

//Function that handle when the user is done with the date picker, and take the value chosen
- (IBAction)doneDOB:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateStyle = NSDateFormatterMediumStyle;
    
    dateOfBirth.text = [NSString stringWithFormat:@"%@",[df stringFromDate:datepick.date]];
    
    datepick.hidden = TRUE;
    doneDOB.hidden = TRUE;
    cancelDOB.hidden = TRUE;
    buttonBackground.hidden = TRUE;
}

//Function that handle the dismission of the date picker
- (IBAction)cancelDOB:(id)sender
{
    dateOfBirth.text = @"Enter your Date of Birth here";
    datepick.hidden = TRUE;
    doneDOB.hidden = TRUE;
    cancelDOB.hidden = TRUE;
    buttonBackground.hidden = TRUE;
}
@end
