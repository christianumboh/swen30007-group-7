//
//  LoginViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 11/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

// This class handles the User Login and User Sign Up Functionality.
#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:NO];
    PFUser *currentUser = [PFUser currentUser];
    
    /* Check if there's a user currently logged in to the system
     * If there's a user logged in, then it will check whether that user already verify his/her email
     * If the user already verified his/her email address and currently logged in then it will skip this view controller
     * Else if show error message, if logged in but still hasn't verified his/her email address.
     * Else just present this login view controller
     */
    if(currentUser)
    {
        BOOL emailVerified = [[currentUser objectForKey:@"emailVerified"] boolValue];
        if(emailVerified)
        {
            PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
            [query whereKey:@"username" equalTo:currentUser.username];
            PFObject *userObject = [query getFirstObject];
            NSString *firstName = [userObject objectForKey:@"firstname"];
            if(firstName.length != 0)
            {
                [self performSegueWithIdentifier:@"proceedSignIn" sender:nil];
            }
            else
            {
                [self performSegueWithIdentifier:@"editProfile" sender:nil];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Verification" message:@"Please verify your email address before proceeding with the login." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
            [alert show];
            [PFUser logOut];
        }
        
    }
    
    // Create the Sign In view controller, with the customized class of SignInGUI
    SignInGUI *logInViewController = [[SignInGUI alloc] init];
    [logInViewController setFields: PFLogInFieldsUsernameAndPassword|
     PFLogInFieldsPasswordForgotten|
     PFLogInFieldsLogInButton|
     PFLogInFieldsSignUpButton];
    [logInViewController setDelegate:self]; // Set ourselves as the delegate
    
    
    // Create the Sign Up view controller, with the customized class of SignUpGUI
    SignUpGUI *signUpViewController = [[SignUpGUI alloc] init];
    [signUpViewController setFields:PFSignUpFieldsDefault|PFSignUpFieldsAdditional];
    [signUpViewController setDelegate:self]; // Set ourselves as the delegate
    
    
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    
    
    // Present the log in view controller
    [self presentViewController:logInViewController animated:NO completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0)
    {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"Missing Information"
                                message:@"Please ensure you fill out all the login credentials!"
                               delegate:nil
                      cancelButtonTitle:@"Dismiss"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

//Handle the SignIn
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user
{
    [self dismissViewControllerAnimated:NO completion:NULL];
}

//Handle the SignUp, user that sign up for an account will automatically generated a new profile for that particular user.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user
{
    PFObject *profile = [PFObject objectWithClassName:@"Profile"];
    [profile setObject:user.username forKey:@"username"];
    [profile setObject:@"" forKey:@"firstname"];
    [profile setObject:@"" forKey:@"lastname"];
    [profile setObject:@"" forKey:@"dateofbirth"];
    [profile setObject:@"" forKey:@"address"];
    [profile save];
    [self dismissViewControllerAnimated:NO completion:NULL];
}

//Handle cancelation of SignUp
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController
{
    NSLog(@"User dismissed the signUpViewController");
}

@end


