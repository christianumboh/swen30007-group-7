//
//  SettingsViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 13/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController
@synthesize AssistedSettingBG;
@synthesize CarerSettingBG;
@synthesize manageCarersButton;
@synthesize manageCarersLabel;
@synthesize manageCarersIcon;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    NSString *userType = [profile objectForKey:@"usertype"];
    
    if([userType isEqualToString:@"Carer"])
    {
        CarerSettingBG.hidden = FALSE;
    }
    else
    {
        AssistedSettingBG.hidden = FALSE;
        manageCarersLabel.hidden = FALSE;
        manageCarersButton.hidden = FALSE;
        manageCarersIcon.hidden = FALSE;
        manageCarersButton.userInteractionEnabled = TRUE;
    }
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)doSignOut
{
    [PFUser logOut];
    [self performSegueWithIdentifier:@"proceedSignOut" sender:nil];
}

- (IBAction)SignOut:(id)sender
{
    [self performSelector:@selector(doSignOut)];
}

- (IBAction)editProfile:(id)sender
{
    [self performSegueWithIdentifier:@"editProfile" sender:nil];
}

- (IBAction)changePassword:(id)sender
{
    [self performSegueWithIdentifier:@"changePassword" sender:nil];
}

- (IBAction)manageCarer:(id)sender
{
    [self performSegueWithIdentifier:@"manageCarer" sender:nil];
}


@end