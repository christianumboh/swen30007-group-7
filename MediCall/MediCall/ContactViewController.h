//
//  ContactViewController.h
//  MediCall
//
//  Created by Christian Umboh on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ContactViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
-(IBAction)back:(id)sender;
@end
