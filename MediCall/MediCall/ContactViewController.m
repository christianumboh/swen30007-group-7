//
//  ContactViewController.m
//  MediCall
//
//  Created by Christian Umboh on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ContactViewController.h"
#import "ContactDetailViewController.h"

@interface ContactViewController ()
@property (strong) NSMutableArray *contacts;

@end

@implementation ContactViewController

/*
 * Function that is triggered when the "Back" button is tapped
 */
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSManagedObjectContext *)managedObjectContext
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    PFUser *currentUser = [PFUser currentUser];
    
    // Fetch the current user's contacts from persistent data store.
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contact"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"owner == %@", currentUser.username];
    [fetchRequest setPredicate:predicate];
    self.contacts = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    [self.tableView reloadData];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.contacts.count;
}

/*
 * Function that handles the content of a cell (contact details)
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *contact = [self.contacts objectAtIndex:indexPath.row];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Display contacts in the cell
    [cell.textLabel setText:[NSString stringWithFormat:@"%@ %@", [contact valueForKey:@"firstName"], [contact valueForKey:@"lastName"]]];
    [cell.detailTextLabel setText:[contact valueForKey:@"phoneNumber"]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"UpdateContact"]) {
        NSManagedObject *selectedContact = [self.contacts objectAtIndex:[[self.tableView indexPathForSelectedRow] row]];
        ContactDetailViewController *destViewController = segue.destinationViewController;
        destViewController.contact = selectedContact;
    }
}

@end