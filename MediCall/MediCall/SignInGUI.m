//
//  SignInGUI.m
//  MediCall
//
//  Created by Aristea Wijaya on 13/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

// This is a class for implementing custom GUI for the SignInView in the LoginViewController

#import "SignInGUI.h"
#import <QuartzCore/QuartzCore.h>

@interface SignInGUI ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation SignInGUI

@synthesize fieldsBackground;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Make the background image centered, and fit the frame
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *BGImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
	// Do any additional setup after loading the view.
    // Set the background Color
    [self.logInView setBackgroundColor:[UIColor colorWithPatternImage:BGImage]];
    // Set the Logo
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"medicall_logo.png"]]];
    // Set the Fields Background
    fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LogInFieldBG.png"]];
    [self.logInView addSubview:self.fieldsBackground];
    [self.logInView sendSubviewToBack:self.fieldsBackground];
    
    // Set the Color of Sign Up label in the Sign Up Button
    [self.logInView.signUpLabel setTextColor:[UIColor whiteColor]];
    
    
    // Remove text shadow
    CALayer *layer = self.logInView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.logInView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    
    // Set the Font/Text Color
    [self.logInView.usernameField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    [self.logInView.passwordField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    // Do the animation
    [self performSelector:@selector(doAnimation)];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    //Set the position of each fields to be centered
    [self.logInView.logo setFrame:CGRectMake(35.5f, 100.0f, 240.0f, 60.0f)];
    [self.logInView.usernameField setFrame:CGRectMake(35.0f, 160.0f, 250.0f, 50.0f)];
    [self.logInView.passwordField setFrame:CGRectMake(35.0f, 210.0f, 250.0f, 50.0f)];
    [self.fieldsBackground setFrame:CGRectMake(35.0f, 160.0f, 250.0f, 100.0f)];
    [self.logInView.passwordForgottenButton setFrame:CGRectMake(15.0f, 185.0f, 25.0f, 50.0f)];
    [self.logInView.logInButton setFrame:CGRectMake(35.0f, 270.0f, 250.0f, 40.0f)];
    [self.logInView.signUpLabel setFrame:CGRectMake(35.0f, 330.0f, 250.0f, 10.0f)];
    [self.logInView.signUpButton setFrame:CGRectMake(35.0f, 350.0f, 250.0f, 40.0f)];

    
}

//Function for animation, fade-in animation
- (void)doAnimation
{
    //logInView..alpha = 0.0f;
    self.logInView.usernameField.alpha = 0.0f;
    self.logInView.passwordField.alpha = 0.0f;
    
    self.logInView.logInButton.alpha = 0.0f;
    
    self.logInView.signUpButton.alpha = 0.0f;
    self.logInView.signUpLabel.alpha = 0.0f;
    
    self.logInView.passwordForgottenButton.alpha = 0.0f;
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.usernameField.alpha = 1.0;}
                     completion:nil];
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.passwordField.alpha = 1.0;}
                     completion:nil];
    
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.logInButton.alpha = 1.0;}
                     completion:nil];
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.passwordForgottenButton.alpha = 1.0;}
                     completion:nil];
    
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.signUpButton.alpha = 1.0;}
                     completion:nil];
    [UIView animateWithDuration:1.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{self.logInView.signUpLabel.alpha = 1.0;}
                     completion:nil];
}

@end
