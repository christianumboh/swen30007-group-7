//
//  main.m
//  MediCall
//
//  Created by Christian Umboh on 20/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
