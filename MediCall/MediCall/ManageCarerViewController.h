//
//  ManageCarerViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 4/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ManageCarerViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *primaryName;
@property (weak, nonatomic) IBOutlet UILabel *primaryPhone;
@property (weak, nonatomic) IBOutlet UILabel *secondaryName;
@property (weak, nonatomic) IBOutlet UILabel *secondaryPhone;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;

@property (weak, nonatomic) UIScrollView *scrollView;
-(IBAction)back:(id)sender;
-(IBAction)editCarer:(id)sender;
@end
