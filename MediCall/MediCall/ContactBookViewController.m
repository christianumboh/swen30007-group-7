//
//  ContactBookViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 26/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ContactBookViewController.h"

@interface ContactBookViewController ()

@end

@implementation ContactBookViewController
{
    NSArray *contactBookData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    contactBookData = [NSArray arrayWithObjects:@"Christian Umboh",
                                                @"Hadrian Pranjoto",
                                                @"Biondi Tantra",
                                                @"Vilberto Noerjanto", nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [contactBookData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [contactBookData objectAtIndex:indexPath.row];
    return cell;
}

@end
