//
//  HomeViewController.m
//  MediCall
//
//  Created by Vilberto Austin Noerjanto on 26/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize CallButton;
@synthesize nameLabel;
@synthesize phoneURL;
@synthesize userType;
@synthesize chosenCarerButton;
@synthesize chosenCarer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    userType = [profile objectForKey:@"usertype"];
    NSMutableString *firstname = [NSMutableString stringWithString:@"Hi, "];
    [firstname appendString:[profile objectForKey:@"firstname"]];
    [firstname appendString:@"!"];
    nameLabel.text = firstname;
    
    //dismiss keyboard when tapping outside textfield
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    // Initialise phoneURL as the primary carer's number
    if([userType isEqualToString:@"Assisted"])
    {
        chosenCarerButton.hidden = FALSE;
        chosenCarer = @"Primary";
        phoneURL = [NSMutableString stringWithString:@"tel://"];
        [phoneURL appendString:[profile objectForKey:@"primarycarerphone"]];
        [chosenCarerButton addTarget:self action:@selector(chooseCarer:) forControlEvents:UIControlEventValueChanged];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* 
 * Function that opens a given telephone url
 */
- (void)openPhoneURL
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURL]];
}

/*
 * Function that handles the calling functionality
 *
 * This function selects the individual to call and does the calling
 * by opening the telephone url using the selector function openPhoneURL
 */
- (IBAction)CallCarer:(id)sender
{
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    
    // Checks if the user is an assisted person or a carer
    if([userType isEqualToString:@"Assisted"])
    {
        // Choose to call the chosen carer of the user (primary or secondary)
        if ([chosenCarer isEqualToString:@"Primary"]) {
            phoneURL = [NSMutableString stringWithString:@"tel://"];
            [phoneURL appendString:[profile objectForKey:@"primarycarerphone"]];
        }
        else {
            phoneURL = [NSMutableString stringWithString:@"tel://"];
            [phoneURL appendString:[profile objectForKey:@"secondarycarerphone"]];
        }
        [self performSelector:@selector(openPhoneURL)];
        NSLog(@"%@", phoneURL);
    }
    
    else
    {
        [self performSegueWithIdentifier:@"callContact" sender:nil];
    }
}

/*
 * Function that extracts the chosen carer value from the segmented control
 */
- (IBAction)chooseCarer:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    if(selectedSegment == 0)
    {
        chosenCarer = @"Primary";
    }
    else
    {
        chosenCarer = @"Secondary";
    }
}

- (IBAction)BackgroundClick:(id)sender
{
    [self.statusField resignFirstResponder];
}

- (void)dismissKeyboard
{
    [self.statusField resignFirstResponder];
}

@end
