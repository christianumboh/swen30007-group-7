//
//  LoginViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 13/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "SignInGUI.h"
#import "SignUpGUI.h"

@interface LoginViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@end
