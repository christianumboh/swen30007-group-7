//
//  HomeViewController.h
//  MediCall
//
//  Created by Vilberto Austin Noerjanto on 26/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface HomeViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *CallButton;
@property (weak, nonatomic) IBOutlet UITextField *statusField;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic) NSMutableString *phoneURL;
@property (nonatomic) NSString *userType;
@property (weak, nonatomic) IBOutlet UISegmentedControl *chosenCarerButton;
@property (nonatomic) NSString *chosenCarer;

- (IBAction)CallCarer:(id)sender;
- (IBAction)chooseCarer:(id)sender;
- (IBAction)BackgroundClick:(id)sender;
@end
