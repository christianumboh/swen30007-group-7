//
//  ContactBookViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 26/08/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactBookViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
