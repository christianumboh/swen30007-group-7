//
//  SignUpGUI.m
//  MediCall
//
//  Created by Aristea Wijaya on 13/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

// This is a class for implementing custom GUI for the SignUpView in the LoginViewController

#import "SignUpGUI.h"
#import <QuartzCore/QuartzCore.h>

@interface SignUpGUI ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation SignUpGUI

@synthesize fieldsBackground;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Make the background image centered, and fit the frame
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background.png"] drawInRect:self.view.bounds];
    UIImage *BGImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //set the background color
    [self.signUpView setBackgroundColor:[UIColor colorWithPatternImage:BGImage]];
    //set the logo
    [self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"medicall_logo.png"]]];
    //set the fields background
    [self setFieldsBackground:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SignUpFieldBG.png"]]];
    [self.signUpView insertSubview:fieldsBackground atIndex:1];
    //set the image for dismiss button
    [self.signUpView.dismissButton setBackgroundImage:[UIImage imageNamed:@"close_icon.png"] forState:UIControlStateNormal];
    
    // Remove text shadow
    CALayer *layer = self.signUpView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.emailField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.additionalField.layer;
    layer.shadowOpacity = 0.0f;
    
    // Set the Text/Font Color
    [self.signUpView.usernameField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    [self.signUpView.passwordField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    [self.signUpView.emailField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    [self.signUpView.additionalField setTextColor:[UIColor colorWithRed:146.0f/255.0f green:1.0f/255.0f blue:1.0f/255.0f alpha:1.0]];
    
    // Set the additional field to be a Phone Number Field
    [self.signUpView.additionalField setPlaceholder:@"Phone Number"];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    // Move all fields down on smaller screen sizes
    // Determine the yOffset, for the yPosition for each field, depend on the iPhone size
    float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;
    
    CGRect fieldFrame = self.signUpView.usernameField.frame;
    fieldFrame.origin.y -= 40;
    
    [self.signUpView.logo setFrame:CGRectMake(35.5f, 60.0f, 240.0f, 60.0f)];
    [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 350.0f, 250.0f, 40.0f)];
    [self.fieldsBackground setFrame:CGRectMake(35.0f, fieldFrame.origin.y + yOffset, 250.0f, 200.0f)];
    
    [self.signUpView.usernameField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset,
                                                       fieldFrame.size.width - 10.0f,
                                                       50.0f)];
    yOffset += fieldFrame.size.height + 5;
    
    [self.signUpView.passwordField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset,
                                                       fieldFrame.size.width - 10.0f,
                                                       50.0f)];
    yOffset += fieldFrame.size.height + 5;
    
    [self.signUpView.emailField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                    fieldFrame.origin.y + yOffset,
                                                    fieldFrame.size.width - 10.0f,
                                                    50.0f)];
    yOffset += fieldFrame.size.height + 3;
    
    [self.signUpView.additionalField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                         fieldFrame.origin.y + yOffset,
                                                         fieldFrame.size.width - 10.0f,
                                                         50.0f)];
}


@end
