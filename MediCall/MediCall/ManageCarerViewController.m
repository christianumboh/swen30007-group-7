//
//  ManageCarerViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 4/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ManageCarerViewController.h"
#import "ContactViewController.h"

@interface ManageCarerViewController ()

@end

@implementation ManageCarerViewController
@synthesize primaryName;
@synthesize primaryPhone;
@synthesize secondaryName;
@synthesize secondaryPhone;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Loads the user profile from Parse
    PFUser *currentUser = [PFUser currentUser];
    PFQuery *query = [PFQuery queryWithClassName:@"Profile"];
    [query whereKey:@"username" equalTo:currentUser.username];
    PFObject *profile = [query getFirstObject];
    
    // Extracts the carer details from Parse and display them
    NSString *primaryCarerName = [profile objectForKey:@"primarycarer"];
    NSString *primaryCarerPhone = [profile objectForKey:@"primarycarerphone"];
    NSString *secondaryCarerName = [profile objectForKey:@"secondarycarer"];
    NSString *secondaryCarerPhone = [profile objectForKey:@"secondarycarerphone"];
    primaryName.text = primaryCarerName.length > 0 ? primaryCarerName : @"Primary Carer Undefined";
    primaryPhone.text = primaryCarerPhone.length > 0 ? primaryCarerPhone : @"";
    secondaryName.text = secondaryCarerName.length > 0? secondaryCarerName : @"Secondary Carer Undefined";
    secondaryPhone.text = secondaryCarerPhone.length > 0? secondaryCarerPhone : @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 * Function that is triggered when the "Back" button is tapped
 */
- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Function that is triggered when the "Edit" button is tapped
 */
- (IBAction)editCarer:(id)sender
{
    [self performSegueWithIdentifier:@"editCarer" sender:nil];
}

@end
