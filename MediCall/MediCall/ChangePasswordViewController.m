//
//  ChangePasswordViewController.m
//  MediCall
//
//  Created by Aristea Wijaya on 1/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController
@synthesize changedPassword; //textfield for new password
@synthesize verifyPassword; //textfield for re-type new password (checking typo)
@synthesize backButton; //button to dismiss the controller
@synthesize saveButton; //button to save the changes made on password


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Dismiss keyboard when tapping outside textfield
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*Function for dismissing Keyboard*/
- (void)dismissKeyboard
{
    [changedPassword resignFirstResponder];
    [verifyPassword resignFirstResponder];
}

/*Function for saving the changes on password
 *the new password made will be pushed to the user login credentials on PARSE cloud database.
 */
- (void)doSave
{
    [PFUser currentUser].password = changedPassword.text;
    [[PFUser currentUser] save];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Change Successful" message:@"Congratulations you have successfully change your password." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
    [alert show];
    [self.navigationController popViewControllerAnimated:YES];
}

/*Function for checking the new password input
 *Will only return true if and only if both textfields (new password & retype new password field) have the same value.
 */
- (BOOL)checkPassword:(NSString *)password1 andRetypePassword:(NSString *)password2
{
    if([password1 isEqualToString:password2])
    {
        return TRUE;
    }
    return FALSE;
}


/*Dismiss Keyboard if user tap on things other than textfields*/
- (IBAction)backgroundClick:(id)sender
{
    [changedPassword resignFirstResponder];
    [verifyPassword resignFirstResponder];
}

/* Function that handle savePassword button's action.
 * It will only call the doSave function if and only if the boolean samePassword return true
 * Else it will return error messages depending on whether it's error by missing input in one or both of the textfields,
 * or different passwords used as input inside the two fields.
 */
- (IBAction)savePassword:(id)sender
{
    if(changedPassword.text.length > 0 && verifyPassword.text.length > 0)
    {
        BOOL samePassword = [self checkPassword:changedPassword.text andRetypePassword:verifyPassword.text];
        if(samePassword)
        {
            [self performSelector:@selector(doSave)];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Mis-Match" message:@"Please ensure you input the exact same password for New Password field and Verify New Password field (case sensitive)." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Field Information" message:@"Please ensure every fields are filled before you save the change." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil,nil];
        [alert show];
    }
}

/* Function that handle the Cancel button,
 * It will simply dismiss the ChangePasswordViewController
 */
- (IBAction)cancelChange:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
