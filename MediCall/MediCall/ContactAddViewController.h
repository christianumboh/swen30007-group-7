//
//  ContactAddViewController.h
//  MediCall
//
//  Created by Christian Umboh on 7/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UserObject.h"

@interface ContactAddViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancel;

- (IBAction)save:(id)sender;
- (IBAction)BackgroundClick:(id)sender;

-(BOOL)validateNewContact:(NSString *)firstName andLastName:(NSString *)lastName andPhoneNumber:(NSString *)phoneNumber;

@end
