//
//  UserObject.m
//  MediCall
//
//  Created by Aristea Wijaya on 10/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

// This class is a mock object for User,
// The aim of creating this class is abling the function in ProfileViewController to have it's unit test.

#import "UserObject.h"

@interface UserObject ()

@end

@implementation UserObject
@synthesize firstName; //represents the First Name of the user in Profile
@synthesize lastName; //represents the Last Name of the user in Profile
@synthesize dateOfBirth; //represents the Date of Birth of the user in Profile
@synthesize address; //represents the Address of the user in Profile
@synthesize phoneNumber; //represents the Phone Number of the user

@end
