//
//  ProfileViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "UserObject.h"

@interface ProfileViewController : UIViewController
{
    UIDatePicker *datepick;
}

@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *userTypeButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirth;
@property (nonatomic, retain) IBOutlet UIDatePicker *datepick;
@property (weak, nonatomic) IBOutlet UIButton *doneDOB;
@property (weak, nonatomic) IBOutlet UIButton *cancelDOB;
@property (weak, nonatomic) IBOutlet UIButton *buttonDOB;
@property (weak, nonatomic) IBOutlet UIImageView *buttonBackground;

- (IBAction)cancelEdit:(id)sender;
- (IBAction)saveProfile:(id)sender;
- (IBAction)chooseUserType:(id)sender;
- (IBAction)backgroundClick:(id)sender;
- (IBAction)buttonDOB:(id)sender;
- (IBAction)doneDOB:(id)sender;
- (IBAction)cancelDOB:(id)sender;

-(BOOL)validateEditProfile:(NSString *)firstname andLastName:(NSString *)lastname andDateOfBirth:(NSString *)dateofbirth andAddress:(NSString *)useraddress;
@end
