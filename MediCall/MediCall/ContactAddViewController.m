//
//  ContactAddViewController.m
//  MediCall
//
//  Created by Christian Umboh on 7/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import "ContactAddViewController.h"

@interface ContactAddViewController ()
@property (strong, nonatomic) UserObject *userObject;
@end

@implementation ContactAddViewController
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNumber;
@synthesize saveButton;
@synthesize cancel;
@synthesize userObject = _userObject;


- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (UserObject *)userObject
{
    if(!_userObject)
    {
        _userObject = [[UserObject alloc] init];
    }
    return _userObject;
}

/*
 * Function that is triggered when the "Cancel" button is tapped
 */
- (IBAction)cancel:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

/*
 * Function that handles the saving when a new contact is added
 * Triggered when the "Save" button is tapped
 */
- (IBAction)save:(id)sender {
    
    // Checks if all text fields are not empty
    if(firstName.text.length > 0 && lastName.text.length > 0 && phoneNumber.text.length > 0)
    {
        BOOL validInformation = [self validateNewContact:firstName.text andLastName:lastName.text andPhoneNumber:phoneNumber.text];
        
        if (validInformation)
        {
            NSManagedObjectContext *context = [self managedObjectContext];
            PFUser *currentUser = [PFUser currentUser];
        
            // Create a new managed object
            NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contact" inManagedObjectContext:context];
            
            // Stores the new contact details
            [newContact setValue:firstName.text forKey:@"firstName"];
            [newContact setValue:lastName.text forKey:@"lastName"];
            [newContact setValue:phoneNumber.text forKey:@"phoneNumber"];
            [newContact setValue:currentUser.username forKey:@"owner"];
        
            NSError *error = nil;
            // Save the object to persistent storage
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
        
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Missing Information!" message:@"Please ensure to fill in all the details." delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil];
        [message show];
    }
}

/*
 * Function that handles testing for adding new contacts
 */
-(BOOL)validateNewContact:(NSString *)inputFirstName andLastName:(NSString *)inputLastName andPhoneNumber:(NSString *)inputPhoneNumber
{
    if(inputFirstName.length > 0 && inputLastName.length > 0 && inputPhoneNumber.length > 0)
    {
        self.userObject.firstName = inputFirstName;
        self.userObject.lastName = inputLastName;
        self.userObject.phoneNumber = inputPhoneNumber;
        
        if([inputFirstName isEqualToString:self.userObject.firstName] &&
           [inputLastName isEqualToString:self.userObject.lastName] &&
           [inputPhoneNumber isEqualToString:self.userObject.phoneNumber])
        {
            return TRUE;
        }
        else
            return FALSE;
    }
    else
        return FALSE;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //dismiss keyboard when tapping outside textfield
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)BackgroundClick:(id)sender
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [phoneNumber resignFirstResponder];
}

- (void)dismissKeyboard
{
    [firstName resignFirstResponder];
    [lastName resignFirstResponder];
    [phoneNumber resignFirstResponder];
}

@end
