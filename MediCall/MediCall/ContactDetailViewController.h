//
//  ContactDetailViewController.h
//  MediCall
//
//  Created by Christian Umboh on 14/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ContactDetailViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic) NSMutableString *carerName;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *primaryCarerButton;
@property (weak, nonatomic) IBOutlet UIButton *secondaryCarerButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *delButton;

@property (strong) NSManagedObject *contact;


- (IBAction)back:(id)sender;
- (IBAction)save:(id)sender;
- (IBAction)edit:(id)sender;


- (IBAction)BackgroundClick:(id)sender;
- (IBAction)setAsPrimary:(id)sender;
- (IBAction)setAsSecondary:(id)sender;
- (IBAction)callContact:(id)sender;

@end
