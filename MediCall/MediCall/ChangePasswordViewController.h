//
//  ChangePasswordViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 1/10/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface ChangePasswordViewController : UIViewController
- (BOOL)checkPassword:(NSString *)password1 andRetypePassword:(NSString *)password2;
@property (weak, nonatomic) IBOutlet UITextField *changedPassword;
@property (weak, nonatomic) IBOutlet UITextField *verifyPassword;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
- (IBAction)savePassword:(id)sender;
- (IBAction)cancelChange:(id)sender;
- (IBAction)backgroundClick:(id)sender;
@end
