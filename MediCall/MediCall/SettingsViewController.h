//
//  SettingsViewController.h
//  MediCall
//
//  Created by Aristea Wijaya on 13/09/13.
//  Copyright (c) 2013 SWEN30004 Group 7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface SettingsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *AssistedSettingBG;
@property (weak, nonatomic) IBOutlet UIImageView *CarerSettingBG;
@property (weak, nonatomic) IBOutlet UIImageView *manageCarersIcon;
@property (weak, nonatomic) IBOutlet UILabel *manageCarersLabel;
@property (weak, nonatomic) IBOutlet UIButton *signOutButton;
@property (weak, nonatomic) IBOutlet UIButton *editProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *manageCarersButton;
- (IBAction)SignOut:(id)sender;
- (IBAction)editProfile:(id)sender;
- (IBAction)changePassword:(id)sender;
- (IBAction)manageCarer:(id)sender;

@end
